# Docker Compose - React, Laravel, Nginx, MySQL
React, Laravel, Nginx,  MySQL with Docker Compose.

## Frontend Container
This container contains NodeJS and React enviroment with the following versions;
- NodeJS 16 Alpine
- React 17

## Backend Container
This container created for backend support. The aim is serving different API endpoints via Laravel. It contains the following software versions;
- PHP 7.4 FPM
- Laravel 8

## MySQL

MySQL Version: 5.7

## Using the Project
Copy .env files 

```
 cp ./todo-list-laravel/.env.example ./todo-list-laravel/.env
 cp ./todo-list-react/.env.example ./todo-list-react/.env
```

Execute the following command and the Docker will build the images for containers;

```
docker-compose build
docker-compose up
```


After the first build, please use the following command for preventing unnecessary builds;

```
docker-compose up --d
```


Run composer install
```
docker-compose exec todolist-backend composer install
```
OR
```
docker exec todolist-backend composer install
```


Generate application key
```
docker-compose exec todolist-backend php artisan key:generate
```


To run all of your outstanding migrations
 
```
docker-compose exec todolist-backend php artisan migrate
```
OR
```
docker exec todolist-backend php artisan migrate
```


Generate a key that will be used to sign JWT token
```
docker-compose exec todolist-backend php artisan jwt:secret
```
OR
```
docker exec todolist-backend php artisan jwt:secret
```


Once this is completed, navigate to http://localhost:3000
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [\App\Http\Controllers\Api\Auth\LoginController::class, 'login'])->name('login');
Route::post('/register', [\App\Http\Controllers\Api\Auth\RegisterController::class, 'register'])->name('register');

Route::group(['middleware' => ['auth:api']], static function () {

    Route::post('/logout', [\App\Http\Controllers\Api\Auth\LoginController::class, 'logout'])->name('logout');
    Route::get('/profile', [\App\Http\Controllers\Api\Auth\LoginController::class, 'userProfile'])->name('profile');
    Route::post('/update-profile', [\App\Http\Controllers\Api\UserController::class, 'updateProfile'])->name('profile.update');

});

Route::group(['middleware' => ['auth:api'], 'prefix' => 'todo-list', 'name'=> 'todoList.'], static function () {

        Route::get('/', [\App\Http\Controllers\Api\TaskController::class, 'index'])->name('index');
        Route::post('/', [\App\Http\Controllers\Api\TaskController::class, 'store'])->name('store');
        Route::put('/{id}/update-task-status', [\App\Http\Controllers\Api\TaskController::class, 'updateStatus'])->name('update.status');
        Route::put('/{id}', [\App\Http\Controllers\Api\TaskController::class, 'update'])->name('update');
        Route::delete('/{id}', [\App\Http\Controllers\Api\TaskController::class, 'destroy'])->name('delete');


});






<?php

namespace App\Repositories;

use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;

/**
 * Class TaskRepository
 * @package App\Repositories
 */
class TaskRepository
{
    /**
     * @var user|null
     */
    protected $user;

    /**
     * @var task|null
     */
    protected $task;

    /**
     * TaskRepository constructor.
     * @param User|null $user
     * @param Task|null $task
     */
    public function __construct(User $user = null, Task $task = null)
    {
        $this->user = $user;
        $this->task = $task;
    }

    /**
     * @param $description
     * @param $priority
     * @return Task
     * @throws \Throwable
     */
    public function create($description)
    {
        $newTask = new Task();

        $newTask->description = $description;
        $newTask->status = Task::STATUS['NEW'];
        $newTask->user()->associate($this->user);
        $newTask->saveOrFail();

        return $newTask;
    }

    /**
     * @param $description
     * @param $priority
     * @return Task|null
     * @throws \Throwable
     */
    public function update($description)
    {
        $this->task->description = $description;
        $this->task->saveOrFail();

        return $this->task;
    }

    /**
     * @throws \Throwable
     */
    public function updateStatus()
    {
        $this->task->status === Task::STATUS['DONE'] ? $this->task->done_at = null : $this->task->done_at = Carbon::now();

        $this->task->status = !$this->task->status;

        $this->task->saveOrFail();
    }

    /**
     * @return bool|null
     */
    public function delete()
    {
        return $this->task->delete();
    }

}

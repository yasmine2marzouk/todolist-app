<?php


namespace App\Repositories;

/**
 * Class CustomResponse
 * @package App\Repositories
 */
class CustomResponse
{
    /**
     * Get the token array structure.
     *
     * @param $token
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public static function respondWithToken($token, $user)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => $user
        ], 200);
    }

}

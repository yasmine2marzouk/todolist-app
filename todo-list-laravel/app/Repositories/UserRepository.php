<?php

namespace App\Repositories;

use App\Models\User;

use Illuminate\Support\Facades\Hash;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository
{
    /**
     * @var User|null
     */
    protected $user;

    /**
     * UserRepository constructor.
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     * @throws \Throwable
     */
    public static function create($name, $email, $password)
    {
        $user = new User();

        $user->name = $name;
        $user->email = strtolower($email);
        $user->password = Hash::make($password);

        $user->saveOrFail();

        return $user;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User|null
     * @throws \Throwable
     */
    public function update($name, $email, $password)
    {
        $this->user->name = $name;
        $this->user->email = $email;

        if($password){
            $this->user->password = Hash::make($password);
        }

        $this->user->saveOrFail();

        return $this->user;
    }

    /**
     * @param $email
     * @return mixed
     */
    public static function getUserByEmail($email)
    {
        return User::query()->where('email', '=', $email)->first();
    }
}

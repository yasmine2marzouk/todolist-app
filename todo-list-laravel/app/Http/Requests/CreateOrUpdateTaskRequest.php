<?php

namespace App\Http\Requests;

use App\Models\Task;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateOrUpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * Allow edit task only if the authenticated user is the owner of the task.
         */
        if ($this->route('id')) {
            $task = Task::find($this->route('id'));
            return $task && Gate::allows('update-task', $task);
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => [
                'nullable',
                'string',
            ]
        ];
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOrUpdateTaskRequest;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;

/**
 * Class TaskController
 * @package App\Http\Controllers\Api
 */
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $user = auth()->user();
        return $user->tasks;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrUpdateTaskRequest $request
     * @param $listId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(CreateOrUpdateTaskRequest $request)
    {
        $user = auth()->user();
        (new TaskRepository($user, null))->create($request->input('description'));
        $user->load('tasks');
        return response()->json(['status' => 'success', 'todoList' => $user->tasks], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $listId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(CreateOrUpdateTaskRequest $request, $id)
    {
        $user = auth('api')->user();

        /**
         * Will fail if the requested task does not belong to the user
         */
        $task = $user->tasks()->findOrFail($id);

        (new TaskRepository($user, $task))->update($request->input('description'));

        $user->load('tasks');

        return response()->json(['status' => 'success', 'todoList' => $user->tasks], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $listId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function updateStatus(Request $request, $id)
    {
        $user = auth('api')->user();

        /**
         * Will fail if the requested task does not belong to the user
         */
        $task = $user->tasks()->findOrFail($id);

        (new TaskRepository($user, $task))->updateStatus();

        $user->load('tasks');

        return response()->json(['status' => 'success', 'todoList' => $user->tasks], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $listId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = auth('api')->user();

        /**
         * Will fail if the requested task does not belong to the user
         */
        $task = $user->tasks()->findOrFail($id);

        $result = (new TaskRepository($user, $task))->delete();

        if ($result) {
            $user->load('tasks');
            return response()->json(["status" => "success", "todoList" => $user->tasks], 200);
        }
        return response()->json(["status" => "Fail", "todoLists" => null], 400);
    }
}

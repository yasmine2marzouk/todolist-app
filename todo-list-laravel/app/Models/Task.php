<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

/**
 * Class Task
 * @package App\Models
 *
 * @property integer user_id
 * @property string description
 * @property integer status
 * @property date done_at
 * @property date created_at
 * @property date updated_at
 *
 * @property-read User user
 */
class Task extends Model
{
    use HasFactory;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'done_at' => 'datetime:d/m/Y H:i',
        'created_at' => 'datetime:d/m/Y H:i',
    ];

    /**
     * Different states of a task
     */
    const STATUS = [
        'NEW'=> 0,
        'DONE'=> 1,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

import { makeStyles } from "@material-ui/core/styles";
import background_2 from "../../assets/images/background_2.png";

const drawerWidth = 400;
const topMenuHeight = 64;

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    height: topMenuHeight,
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    [theme.breakpoints.up("md")]: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
    },

    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {},
  menuIcon: {
    width: "35px !important",
    height: "35px !important",
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: `${theme.palette.primary.main} !important`,
  },
  drawerOpen: {
    width: drawerWidth,
    backgroundColor: `${theme.palette.primary.main} !important`,
    transition: `${theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })} !important`,
  },
  drawerClose: {
    backgroundColor: `${theme.palette.primary.main} !important`,
    transition: `${theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })} !important`,
    overflowX: "hidden",
    width: 0,
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    minHeight: "100%",
    overflow: "hidden",
    background: `url(${background_2})no-repeat center center fixed`,
    backgroundSize: "cover",
    transition: theme.transitions.create("margin-left", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  contentShift: {
    [theme.breakpoints.down("sm")]: {
      flexGrow: 1,
      minHeight: "100%",
      overflow: "hidden",
      backgroundColor: "white",
      backgroundImage: `url(${background_2})`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "right bottom",
      backgroundAttachment: "fixed",
      transition: theme.transitions.create("margin-left", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    [theme.breakpoints.up("md")]: {
      flexGrow: 1,
      minHeight: "100%",
      overflow: "hidden",
      backgroundColor: "white",
      backgroundImage: `url(${background_2})`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "right bottom",
      backgroundAttachment: "fixed",
      marginLeft: drawerWidth,
      transition: theme.transitions.create("margin-left", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
  },
  children: {
    width: "100%",
    minHeight: `calc(100vh - ${topMenuHeight}px)`,
    overflow: "hidden",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  closeButton: {
    color: "white",
  },
  menuContainer: {
    marginLeft: 75,
    display: "flex",
    flex: 1,
    flexDirection: "column",
    height: "100%",
  },
  li: {
    display: "flex",
    alignItems: "center",
    marginTop: 5,
    marginBottom: 5,
    textDecoration: "none",
    cursor: "pointer",
    "&:hover": {
      "&>h1": {
        left: 10,
        transition: theme.transitions.create("left", {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
    },
  },
  appBarContent: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  logoContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  liTitle: {
    position: "relative",
    left: 0,
    color: "white",
    fontSize: 18,
    fontFamily: "Signika-Medium",
    transition: theme.transitions.create("left", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  liIcon: {
    width: "25px !important",
    height: "25px !important",
    color: "white",
    marginRight: 5,
  },
  logo: {
    width: 200,
    height: 100,
  },
  icon: {
    width: 160,
    height: 24,
  },
  ul: {
    padding: 0,
    display: "flex",
    flex: 1,
    flexDirection: "column",
  },
  button: {
    backgroundColor: "white",
    color: `${theme.palette.primary.main} !important`,
    fontSize: 14,
    fontFamily: "Signika-Bold",
    letterSpacing: 2.8,
    marginTop: 25,
    marginBottom: 25,
    width: 165,
    height: 40,
    textAlign: "center",
    "&:hover": {
      backgroundColor: "#f0f8ff",
    },
  },
}));

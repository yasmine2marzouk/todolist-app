import React, { ReactNode} from "react";
import {
    CssBaseline,
} from "@material-ui/core";
import { useStyles } from "./style";
import { CustomAppBar } from "./CustomAppBar";

interface PrivateLayoutPropsInterface {
    children: ReactNode //content
}

export const PrivateLayout = React.memo((props: PrivateLayoutPropsInterface) => {
    // style classes
    const classes = useStyles();
    // destructing props
    const { children } = props;

    return (
        <div>
            <CssBaseline />
            <CustomAppBar/>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                <div className={classes.children}>
                    {children}
                </div>
            </main>
        </div>
    );
})
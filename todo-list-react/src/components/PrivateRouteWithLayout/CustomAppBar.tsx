import React, {useCallback} from 'react';
import {AppBar, IconButton, Toolbar} from "@material-ui/core";
import {ExitToApp} from "@material-ui/icons";
import {useStyles} from "./style";
import {logout} from "../../store/auth/actions";
import {useDispatch} from 'react-redux';
import {useTranslation} from "react-i18next";
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';

export const CustomAppBar = React.memo(() => {
    const classes = useStyles();
    const dispatch = useDispatch();
    // translation hooks
    const {t} = useTranslation();

    // logout
    const onLogout = useCallback(() => {
        dispatch(logout())
    },[]);

    return (<AppBar
        position="fixed"
    >
        <Toolbar className={classes.appBarContent}>
            <div className={classes.logoContainer}>
                <PlaylistAddCheckIcon fontSize={"large"}/> {t('global.appName')}
            </div>

            <IconButton
                color="inherit"
                onClick={onLogout}
                edge="end"

                className={classes.menuButton}
            >

                <ExitToApp fontSize="large"/> {t('auth.logout')}
            </IconButton>
        </Toolbar>
    </AppBar>)
});
import React from 'react';
import { Redirect, Route } from 'react-router';
import { PrivateLayout } from './PrivateLayout';
import {TOKEN} from "../../utils/constants";
import {LOGIN_PATH} from "../../utils/paths";

interface PrivateRouteWithLayoutPropsInterface {
    path: string;
    exact?: boolean;
    component: React.ComponentType,
}

export const PrivateRouteWithLayout = React.memo((props: PrivateRouteWithLayoutPropsInterface) => {
    const {
        component: Component, // view to show
        path, // view path
        exact = false, // exact path or not
    } = props;
    return localStorage.getItem(TOKEN) !== null ? (<Route
        path={path}
        exact={exact}
        render={() => <PrivateLayout >
            <Component />
        </PrivateLayout>
        }
    />) : <Redirect to={LOGIN_PATH} />
})
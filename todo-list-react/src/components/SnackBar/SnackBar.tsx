import { Snackbar as SnackBarComponent } from "@material-ui/core";
import React, { useCallback } from "react";
import MuiAlert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/rootReducers";
import { hideSnackBar } from "../../store/snackBar/actions";


 const SnackBar = React.memo(() => {
    // get redux snack bar state
    const snackBar = useSelector((state: RootState) => state.snackBar);
    // get dispatch hooks
    const dispatch = useDispatch();
    // handle close snack bar
    const handleClose = useCallback(() => {
        dispatch(hideSnackBar());
    }, [dispatch]);
    return (
        <SnackBarComponent
            open={snackBar.open}
            autoHideDuration={3000}
            onClose={handleClose}
            anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
            }}
        >
            <MuiAlert
                elevation={6}
                variant="filled"
                onClose={handleClose}
                severity={snackBar.severity}
            >
                {snackBar.message}
            </MuiAlert>
        </SnackBarComponent>
    );
}
);
 export default SnackBar;

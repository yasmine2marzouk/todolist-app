import * as React from "react";
import { useStyles } from "./style";

interface TextInputProps {
  value: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  error?: string | null;
  label: string;
}

const TextInput = (props: TextInputProps) => {
  const { value, onChange, error = null, label } = props;
  const classes = useStyles();
  return (
    <React.Fragment>
      <div className={classes.container}>
        <p>{label}</p>
        <input value={value} onChange={onChange} className={classes.input} />
      </div>
      {error && <p>{error}</p>}
    </React.Fragment>
  );
};

export default TextInput;

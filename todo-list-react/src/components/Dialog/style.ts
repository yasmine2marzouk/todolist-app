import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  iconButtonStyle: {
    position: "sticky",
    left: "98%",
  },
  dialogContentStyle: { marginBottom: 15 },
  dialogTitle: {
    fontWeight: "bold",
    fontSize: "26px",
    display: "flex",
    flex: 1,
  },
  dialogTitleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonCancel: {
    color: "#e74a3b",
    backgroundColor: "transparent",
    margin: theme.spacing(1),
    borderStyle: "solid",
    borderWidth: "1px",
    borderColor: "#e74a3b",
    "&:hover": {
      color: "white",
      backgroundColor: "#e74a3b",
    },
  },
  buttonContinue: {
    color: "#1cc88a",
    backgroundColor: "transparent",
    margin: theme.spacing(1),
    borderStyle: "solid",
    borderWidth: "1px",
    borderColor: "#1cc88a",
    "&:hover": {
      color: "white",
      backgroundColor: "#1cc88a",
    },
  },
  disabledButton: {
    color: "#808080",
    backgroundColor: "#D3D3D3",
  },
}));

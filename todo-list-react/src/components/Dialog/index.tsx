import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    IconButton,
    LinearProgress,
    Typography,
} from "@material-ui/core";
import {Cancel} from "@material-ui/icons";
import React from "react";
import {useStyles} from "./style";

interface DialogComponentProps {
    children: React.ReactElement;
    title: string;
    open: boolean;
    spinner?: boolean;
    handleClose: (e: {}) => void;
    handleContinue?: () => void;
    handleCancel?: () => void;
    continueTitle?: string;
    cancelTitle?: string;
}

const DialogComponent = (props: DialogComponentProps) => {
    const {
        children,
        title,
        open,
        handleClose,
        spinner,
        handleContinue = undefined,
        handleCancel = undefined,
        continueTitle = undefined,
        cancelTitle = undefined,
    } = props;
    const classes = useStyles();
    return (
        <div>
            <Dialog maxWidth="lg" open={open} onClose={handleClose}>
                <DialogTitle
                    className={classes.dialogTitleContainer}
                    disableTypography={true}
                >
                    <Typography className={classes.dialogTitle}>{title}</Typography>
                    <IconButton className={classes.iconButtonStyle} onClick={handleClose}>
                        <Cancel color="primary"/>
                    </IconButton>
                </DialogTitle>
                <DialogContent className={classes.dialogContentStyle}>
                    {children}
                </DialogContent>
                {spinner === false && <Divider/>}
                {spinner === true && <LinearProgress color="primary"/>}
                {((handleContinue !== undefined && continueTitle !== undefined) ||
                    (handleCancel !== undefined && cancelTitle !== undefined)) && (
                    <DialogActions>
                        {handleContinue !== undefined && continueTitle !== undefined && (
                            <Button
                                onClick={handleContinue}
                                className={
                                    spinner ? classes.disabledButton : classes.buttonContinue
                                }
                                disabled={spinner}
                                autoFocus
                            >
                                {continueTitle}
                            </Button>
                        )}
                        {handleCancel !== undefined && cancelTitle !== undefined && (
                            <Button onClick={handleCancel} className={classes.buttonCancel}>
                                {cancelTitle}
                            </Button>
                        )}
                    </DialogActions>
                )}
            </Dialog>
        </div>
    );
};

export default DialogComponent;

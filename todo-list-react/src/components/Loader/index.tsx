import { CircularProgress, Container } from "@material-ui/core";
import * as React from "react";
import { useStyles } from "./style";

import Logo from "../../assets/images/logo.png";

interface LoaderComponentProps {}

const LoaderComponent = (props: LoaderComponentProps) => {
  const classes = useStyles();
  return (
    <Container className={classes.container}>
      <div className={classes.subContainer}>
        <img alt={"..."} src={Logo} className={classes.image} />
        <CircularProgress />
      </div>
    </Container>
  );
};

export default LoaderComponent;

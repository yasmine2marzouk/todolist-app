import { makeStyles } from "@material-ui/core/styles";
import background_2 from "../../assets/images/background_2.png";
import background_1 from "../../assets/images/logo.png";

export const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "100vh",
  },
  logoContainer: {
    display: "flex",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundImage: `url(${background_1})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: "100vh",
    backgroundColor: theme.palette.primary.main,
    position: "sticky",
    top: 0,
  },
  logo: {
    width: "30vw",
    height: 100,
  },
  childContainer: {
    minHeight: "100vh",
    display: "flex",
    flex: 1,
    overflow: "hidden",
    background: `url(${background_2})no-repeat center center fixed`,
    backgroundSize: "cover",
    flexDirection: "column",
  },
  langContainer: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    marginRight: 24,
    height: 64,
  },
}));

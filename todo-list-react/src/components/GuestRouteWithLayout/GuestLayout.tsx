import React, { ReactNode } from 'react';
import { Grid } from '@material-ui/core';
import { useStyles } from './style';

interface GuestLayoutPropsInterface {
    children: ReactNode; // content
}

export const GuestLayout = React.memo((props: GuestLayoutPropsInterface) => {
    const { children } = props;
    const classes = useStyles();
    return (<div className={classes.root}>
        <Grid container>
            <Grid item xs={12} md={12}>
                <div className={classes.childContainer}>
                    {children}
                    <div className={classes.langContainer} />
                </div>
            </Grid>
        </Grid>
    </div>)
})
import React from 'react';
import {Redirect, Route} from 'react-router';
import {GuestLayout} from './GuestLayout';
import {TOKEN} from "../../utils/constants";
import {TODO_LIST_PATH} from "../../utils/paths";

interface GuestRouteWithLayoutPropsInterface {
    path: string;
    exact?: boolean;
    component: React.ComponentType,
}

export const GuestRouteWithLayout = React.memo((props: GuestRouteWithLayoutPropsInterface) => {
    const {
        component: Component, // view to show
        path, // view path
        exact = false, // exact path or not
    } = props;
    return (<Route
        path={path}
        exact={exact}
        render={() =>
            localStorage.getItem(TOKEN) === null ?
                (
                    <GuestLayout>
                        <Component/>
                    </GuestLayout>
                )
                : <Redirect to={TODO_LIST_PATH}/>
        }
    />)
})
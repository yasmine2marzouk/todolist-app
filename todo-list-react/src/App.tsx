import React, {useEffect, useState} from "react";
import "./App.css";
import "./locales";
import {applyMiddleware, compose, createStore} from "redux";
import {rootReducer} from "./store/rootReducers";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import {history} from "./utils/history";
import {Routes} from './routes';
import {checkTokenExpirationMiddleware} from "./middleware/auth";
import {api} from "./api";
import {TOKEN} from "./utils/constants";
import { Splash } from './views';

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk, checkTokenExpirationMiddleware))
);

function App() {

    // splash screen
    const [splash, setSplash] = useState<boolean>(true);

    // check if token is still valid
    useEffect(() => {
        const getAuth = async () => {
            const token = localStorage.getItem(TOKEN);
            if (token === null) {
                setTimeout(() => {
                    setSplash(false)
                }, 2000)
            } else {
                try {
                    await api.auth.getAuth();
                    setSplash(false);
                } catch (error) {
                    localStorage.removeItem(TOKEN);
                    setSplash(false);
                }
            }
        };
        getAuth()
    }, []);

    return (
        <Provider store={store}>
            {splash
                ? <Splash /> :
                <Router history={history}>
                    <Routes />
                </Router>
            }
        </Provider>
    );
}

export default App;

// common interfaces
export interface InputInterface<T> {
  value: T;
  error: string | null;
}


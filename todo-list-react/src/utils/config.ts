import {TODO_LIST_PATH} from "./paths";

const SECTION_TODOS = "TODOS";

export const SECTION_TODOS_ID = 2;

const SECTION_TODOS_TITLE = "Todos";

const SECTIONS = [
  { id: SECTION_TODOS_ID, name: SECTION_TODOS , title: SECTION_TODOS_TITLE, path:TODO_LIST_PATH},
];

export const config = {
  BASE_URL_API: process.env.REACT_APP_BASE_URL_API,
  SECTIONS,
};

import { config } from "./config";
import { AxiosRequestConfig } from "axios";

export const TOKEN = "TOKEN";
export const USER = "USER";

// axios config
export const AXIOS_CONFIG: AxiosRequestConfig = {
    baseURL: config.BASE_URL_API,
    timeout: 5000,
    headers: {
        Accept: "application/json",
    },
};
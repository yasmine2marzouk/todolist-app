export const LOGIN_ENDPOINT = "/api/login";
export const GET_AUTH_ENDPOINT = "/api/profile";
export const LOGOUT_ENDPOINT = "/api/logout";
export const SIGN_UP_ENDPOINT = "/api/register";
export const FETCH_TASKS_ENDPOINT = "/api/todo-list";
export const DELETE_TASK_ENDPOINT = "/api/todo-list/{id}";
export const UPDATE_TASK_ENDPOINT = "/api/todo-list/{id}";
export const UPDATE_TASK_STATUS_ENDPOINT = "/api/todo-list/{id}/update-task-status";







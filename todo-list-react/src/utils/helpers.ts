import axios, {
  AxiosError,
  AxiosRequestConfig,
  AxiosResponse,
  Method,
} from "axios";
import { config } from "./config";
import { TOKEN } from "./constants";
import {history} from "./history";
import {LOGIN_PATH} from "./paths";

export const sendAsyncRequest = async (
  method: Method,
  endPoint: string,
  data: any,
  headers: boolean,
  callback: (
    error: AxiosError | undefined,
    response: AxiosResponse | undefined
  ) => void
) => {

  const baseURL = config.BASE_URL_API;

  let _config: AxiosRequestConfig = {
    url: endPoint,
    method,
    baseURL,
    data,
    timeout: 20000,
    headers: {
      Accept: "application/json",
    },
  };
  if (headers) {
    const token = localStorage.getItem(TOKEN);
    _config.headers = { ..._config.headers, Authorization: `Bearer ${token}` };
  }
  try {
    //const dispatch = useDispatch();
    const response = await axios(_config);
    callback(undefined, response);
  } catch (error) {
    if(error.response?.status === 401){
      localStorage.clear();
      history.push(LOGIN_PATH)
    }else if(error.response?.status === 403){
      alert('Unauthorized');
      localStorage.clear();
      history.push(LOGIN_PATH)
    }else{
      console.log("error = ", error);
      callback(error, undefined);
    }
  }
};

export const isEmail = (email: string): boolean => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email));
};

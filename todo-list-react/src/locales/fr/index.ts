import { auth } from "./auth";
import { validation } from "./validation";
import { user } from "./user";
import { global } from "./global";

const locales = {
  auth: auth,
  validation: validation,
  global: global,
  user: user,
};

export default locales;

export const auth = {
    login: "Connexion",
    logout: "Déconnexion",
    passwordConfirmation: "Confrimation de mot de passe",
    rememberMe: "Se souvenir de moi",
    signUpHere: "Ou inscrivez-vous ici",
    signUp: "S'inscrire",
    signIn: "Vous avez déjà un compte? se connecter",
};

export const validation = {
    name: {
        required: "Le champ Nom est obligatoire !",
    },
    email: {
        required: "L'adresse email est obligatoire !",
        email: "L'adresse email n'est pas valide",
        exists: "L'adresse email n'existe pas !",
        unique: "l'adresse email est déjà utilisée !",
    },
    password: {
        required: "Mot de passe est obligatoire !",
        confirmed: "Le champ confirmation de mot de passe ne correspond pas.",
    },
    password_confirmation: {
        confirmation: "Le champ de confirmation mot de passe ne correspond pas !",
    },
    remember: {
        boolean: "Le champ 'Se souvenir de moi' n'est pas valid",
    },
    description: {
        required: "La description de la tâche est obligatoire!"
    },
    auth:{
        failed: "Ces identifiants ne correspondent pas à nos enregistrements."
    }
};
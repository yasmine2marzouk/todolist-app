export const global = {
    appName: "TODO LIST",
    connexionError: "Un problème est survenu! Merci d'essayer plutard.",
    editSuccess: "Mise à jour effectuée avec succès.",
    add: "Enregistrer",
    cancel: "Annuler",
    update: "Modifier",
    delete: "Supprimer",
    deleteTask: "Supprimer une tâche",
    updateTask: "Modifier une tâche",
    app_name: "Todo List ReactJS",
    TODOS: "Mes tâches à faire",
    description: "Ajouter une tâche...",
    nothingToShowTitle:"Liste Vide!!",
    nothingToShowBody:"Votre TODO List est vide! Ajoutez des tâches à faire.",
    areYouSure: "Voulez-vous vraiment supprimer cette tâche ?",
    doneAt: "Terminée le"
};

import {FETCH_TASKS, TaskActionTypes, TaskState} from "./types";
import {AuthActionTypes, LOGOUT} from "../auth/types";


const INITIAL_STATE: TaskState = {
  data: [],
};
export const taskReducer = (
    state = INITIAL_STATE,
    action: TaskActionTypes | AuthActionTypes
): TaskState => {
  switch (action.type) {
    case FETCH_TASKS:
      return {
        ...state,
        data: action.payload,
      };
    case LOGOUT:
      return INITIAL_STATE;
    default:
      return state;
  }
};


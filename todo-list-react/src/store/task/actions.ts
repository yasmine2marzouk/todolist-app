import {ThunkDispatch} from "redux-thunk";
import {RootState} from "../rootReducers";
import {FETCH_TASKS, TaskActionTypes,} from "./types";
import {api} from "../../api";

export const fetchTasks = () => async (
    dispatch: ThunkDispatch<RootState, undefined, TaskActionTypes>
) => {
  try {
    const tasks = await api.task.fetch();
    dispatch({
      type: FETCH_TASKS,
      payload: tasks,
    });
  } catch (error) {
    console.log(error)
  }
};

export const createTask = (description : string) => async (
    dispatch: ThunkDispatch<
        RootState,
        undefined,
        TaskActionTypes
        >
) => {
  try {
    // create task , task response is normalized
    const normalizedTasks = await api.task.create({description: description});
    // dispatch tasks
    dispatch({
      type: FETCH_TASKS,
      payload: normalizedTasks.entities.tasks,
    });
  } catch (error) {}
};

export const updateTask = (id: number, description : string) => async (
    dispatch: ThunkDispatch<
        RootState,
        undefined,
        TaskActionTypes
        >
) => {
  try {
    // update task
    const tasks = await api.task.updateTask(id, {description: description});
    // dispatch tasks
    dispatch({
      type: FETCH_TASKS,
      payload: tasks.todoList,
    });
  } catch (error) {
    console.log(error)
  }
};

export const deleteTask = (id: number) => async (
    dispatch: ThunkDispatch<
        RootState,
        undefined,
        TaskActionTypes
        >
)=> {
  try {
    // delete task
    const tasks = await api.task.removeTask(id);
    // dispatch tasks
    dispatch({
      type: FETCH_TASKS,
      payload: tasks.todoList,
    });
  } catch (error) {
    console.log(error)
  }
};

export const updateStatus = (id: number) => async (
    dispatch: ThunkDispatch<
        RootState,
        undefined,
        TaskActionTypes
        >
)=> {
  try {
    // update task status, task response is normalized
    const tasks = await api.task.updateStatus(id);
    // dispatch tasks
    dispatch({
      type: FETCH_TASKS,
      payload: tasks.todoList,
    });
  } catch (error) {
    console.log(error)
  }
};
// types
import {Task} from "../../models/Task";


export const FETCH_TASKS = "tasks/fetch";
export const DELETE_TASK = "tasks/delete";
export const UPDATE_TASK = "tasks/update";


// state(reducer) interfaces
export interface TaskState {
    data: Array<Task>;
}

// fetch tasks action
interface FetchTasksAction {
    type: typeof FETCH_TASKS;
    payload: Task[];
}

// delete task action
export interface DeleteTaskAction {
    type: typeof DELETE_TASK;
    payload: string;
}

// update task action
export interface UpdateTaskAction {
    type: typeof UPDATE_TASK;
    payload: {taskId: string | number; task: Task};
}

export type TaskActionTypes =
    FetchTasksAction |
    DeleteTaskAction |
    UpdateTaskAction
    ;
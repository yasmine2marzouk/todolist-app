import { combineReducers } from "redux";
import { authReducer } from "./auth/reducer";
import { snackBarReducer } from "./snackBar/reducer";
import { taskReducer } from "./task/reducer";

export const rootReducer = combineReducers({
  auth: authReducer,
  snackBar: snackBarReducer,
  task: taskReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

import {AxiosError, AxiosResponse} from "axios";
import {ThunkDispatch} from "redux-thunk";
import {TOKEN, USER} from "../../utils/constants";
import {GET_AUTH_ENDPOINT, LOGIN_ENDPOINT, LOGOUT_ENDPOINT, SIGN_UP_ENDPOINT,} from "../../utils/endpoints";
import {sendAsyncRequest} from "../../utils/helpers";
import {history} from "../../utils/history";
import {LOGIN_PATH, TODO_LIST_PATH} from "../../utils/paths";
import {RootState} from "../rootReducers";
import {AuthActionTypes, LOGIN_ERROR, LOGIN_SUCCESS, LoginForm, LOGOUT, SIGN_UP_ERROR, SignUpForm,} from "./types";

/**
 * Sign in / login
 *
 *
 * @param {LoginForm}  data: login_data
 * @param {Function | undefined}  callback: function to execute when login done with success
 * @returns function
 */
export const login = (
    data: LoginForm,
    callback: (isSucess: boolean, axios: AxiosError | AxiosResponse) => void
) => async (dispatch: ThunkDispatch<RootState, undefined, AuthActionTypes>) => {
    sendAsyncRequest("POST", LOGIN_ENDPOINT, data, false, (error, response) => {
        if (response !== undefined) {
            localStorage.setItem(TOKEN, response.data.access_token);
            localStorage.setItem(USER, JSON.stringify(response.data.user));

            dispatch({
                type: LOGIN_SUCCESS,
                payload: {
                    email: response.data.user.email,
                    id: response.data.user.id,
                    name: response.data.user.name

                },
            });
            callback(true, response);
            history.push(TODO_LIST_PATH)
        }
        if (error !== undefined) {
            dispatch({
                type: LOGIN_ERROR,
            });
            callback(false, error);
        }
    });
};

/**
 * Sign up
 *
 *
 * @param {SignUpForm}  data: sign_up_data
 * @param {Function | undefined}  callback: function to execute when sign up done with success
 * @returns function
 */
export const signUp = (
    data: SignUpForm,
    callback: (isSucess: boolean, axios: AxiosError | AxiosResponse) => void
) => async (dispatch: ThunkDispatch<RootState, undefined, AuthActionTypes>) => {
    sendAsyncRequest("POST", SIGN_UP_ENDPOINT, data, false, (error, response) => {
        if (response !== undefined) {
            localStorage.setItem(TOKEN, response.data.access_token);
            localStorage.setItem(USER, JSON.stringify(response.data.user));

            dispatch({
                type: LOGIN_SUCCESS,
                payload: {
                    email: response.data.user.email,
                    id: response.data.user.id,
                    name: response.data.user.name
                },
            });
            callback(true, response);
        }
        if (error !== undefined) {
            dispatch({
                type: SIGN_UP_ERROR,
            });
            callback(false, error);
        }
    });
};

/**
 * get auth user
 *
 * @param callback
 */
export const getAuth = (
    callback: (isSucess: boolean, axios: AxiosError | AxiosResponse) => void
) => async (dispatch: ThunkDispatch<RootState, undefined, AuthActionTypes>) => {
    sendAsyncRequest("GET", GET_AUTH_ENDPOINT, null, true, (error, response) => {
        if (response !== undefined) {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: {
                    email: response.data.user.email,
                    id: response.data.user.id,
                },
            });
            callback(true, response);
        }
        if (error !== undefined) {
            if (error.response?.status === 401) {
                localStorage.clear();
                history.push(LOGIN_PATH)
            } else {
                dispatch({
                    type: LOGIN_ERROR,
                });
                callback(false, error);
            }

        }
    });
};

/**
 * logout
 *
 * @returns function
 */
export const logout = () => async (
    dispatch: ThunkDispatch<RootState, undefined, AuthActionTypes>
) => {
    history.push(LOGIN_PATH);
    sendAsyncRequest("POST", LOGOUT_ENDPOINT, null, true, (error, response) => {
        localStorage.clear();
        dispatch({
            type: LOGOUT,
        });
    });
};

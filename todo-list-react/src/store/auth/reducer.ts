import { AuthActionTypes, AuthState, LOGIN_SUCCESS, LOGOUT } from "./types";

const INITIAL_STATE: AuthState = {
  email: "",
  id: 0,
};
export const authReducer = (
  state = INITIAL_STATE,
  action: AuthActionTypes
): AuthState => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        email: action.payload.email,
        id: action.payload.id,
      };

    case LOGOUT:
      return INITIAL_STATE;
    default:
      return state;
  }
};

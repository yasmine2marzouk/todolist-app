// types

export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const SIGN_UP_SUCCESS = "SIGN_UP_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const SIGN_UP_ERROR = "SIGN_UP_ERROR";
export const LOGOUT = "LOGOUT";

// interfaces used in action interfaces

export interface User {
  id: number;
  email: string;
}

// state(reducer) interfaces

export interface AuthState extends User {}

export interface LoginForm {
  email: string;
  password: string;
  remember?: boolean;
}
export interface SignUpForm {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
}

interface UserLoginSuccessAction {
  type: typeof LOGIN_SUCCESS;
  payload: User;
}

interface UserLoginErrorAction {
  type: typeof LOGIN_ERROR;
}

interface UserSignUpErrorAction {
  type: typeof SIGN_UP_ERROR;
}

export interface UserLogoutAction {
  type: typeof LOGOUT;
}

export interface ResetPasswordForm {
  email: string,
  password: string,
  password_confirmation: string
}

// auth actions interfaces

export type AuthActionTypes =
  | UserLoginSuccessAction
  | UserSignUpErrorAction
  | UserLoginErrorAction
  | UserLogoutAction;

export interface Task {
  id: number;
  user_id: number;
  description: string;
  status: number;
  done_at: string;
}

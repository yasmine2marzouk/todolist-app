import React, {Fragment} from "react";
import {Redirect, Switch} from "react-router-dom";
import {GuestRouteWithLayout, PrivateRouteWithLayout, SnackBar} from '../components';
import {Login, SignUp, TodoList} from "../views";
import {LOGIN_PATH, SIGN_UP_PATH, TODO_LIST_PATH} from "../utils/paths";


export const Routes = React.memo(() => {
    return (<Fragment>
            <Switch>
                <GuestRouteWithLayout path={LOGIN_PATH} component={Login} exact/>
                <GuestRouteWithLayout path={SIGN_UP_PATH} component={SignUp} exact/>
                <PrivateRouteWithLayout path={TODO_LIST_PATH} component={TodoList} exact/>
                <Redirect to={LOGIN_PATH}/>
            </Switch>
            <SnackBar/>
        </Fragment>
    )
})

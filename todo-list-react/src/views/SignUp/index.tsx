import {Box, Button, CircularProgress, Container, CssBaseline, Grid, TextField, Typography,} from "@material-ui/core";
import React, {useCallback, useState} from "react";
import {useTranslation} from "react-i18next";
import {useDispatch} from "react-redux";
import Link from '@material-ui/core/Link';
import {InputInterface} from "../../utils/common";
import {isEmail, sendAsyncRequest} from "../../utils/helpers";
import {history} from "../../utils/history";
import {LOGIN_PATH, TODO_LIST_PATH} from "../../utils/paths";
import {useStyles} from "./style";
import {TOKEN, USER} from "../../utils/constants";
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {SIGN_UP_ENDPOINT} from "../../utils/endpoints";
import {LOGIN_SUCCESS} from "../../store/auth/types";
import {showSnackBar} from "../../store/snackBar/actions";

interface SignUpProps {
}

const SignUp = (props: SignUpProps) => {
    const classes = useStyles();
    const {t} = useTranslation();
    const dispatch = useDispatch();
    const [name, setName] = useState<InputInterface<string>>({
        value: "",
        error: null,
    });
    const [email, setEmail] = useState<InputInterface<string>>({
        value: "",
        error: null,
    });
    const [password, setPassword] = useState<InputInterface<string>>({
        value: "",
        error: null,
    });
    const [passwordConfirmation, setPasswordConfirmation] = useState<InputInterface<string>>({
        value: "",
        error: null,
    });

    const [loader, setLoader] = useState<boolean>(false);

    const onChange = useCallback(
        (
            event: React.ChangeEvent<HTMLInputElement>,
            setState: React.Dispatch<React.SetStateAction<InputInterface<string>>>
        ) => {
            setState((e) => ({...e, value: event.target.value, error: null}));
        },
        []
    );

    const handleSubmit = (): void => {
        if (name.value === "")
            setName({...name, error: t("validation.name.required")});

        if (email.value === "")
            setEmail({...email, error: t("validation.email.required")});

        if (email.value !== "" && !isEmail(email.value))
            setEmail({...email, error: t("validation.email.email")});

        if (password.value === "")
            setPassword({...password, error: t("validation.password.required")});

        if (passwordConfirmation.value === "" || passwordConfirmation.value !== password.value) {
            setPasswordConfirmation({
                ...passwordConfirmation,
                error: t("validation.password_confirmation.confirmation")
            });
        }
        if (email.value !== "" && name.value !== "" && isEmail(email.value) && password.value !== "") {
            if (!loader){
                setLoader(true);
                sendAsyncRequest("POST",
                    SIGN_UP_ENDPOINT,
                    {
                        name: name.value,
                        email: email.value,
                        password: password.value,
                        password_confirmation: passwordConfirmation.value,
                    },
                    false,
                    (error, response) => {
                        if (response !== undefined) {
                            localStorage.setItem(TOKEN, response.data.access_token);
                            localStorage.setItem(USER, JSON.stringify(response.data.user));

                            dispatch({
                                type: LOGIN_SUCCESS,
                                payload: {
                                    email: response.data.user.email,
                                    id: response.data.user.id,
                                    name: response.data.user.name

                                },
                            });
                            setLoader(false);
                            history.push(TODO_LIST_PATH)
                        }
                        if (error !== undefined) {
                            setLoader(false);
                            dispatch(showSnackBar(t("global.connexionError"), "error"));
                        }
                    });

            }
        }
    };

    function Copyright() {
        return (
            <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                <Link color="inherit" href="">
                    {t('global.app_name')}
                </Link>{' '}
                {new Date().getFullYear()}
                {'.'}
            </Typography>
        );
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    {t("auth.signUp")}
                </Typography>
                <div className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12}>
                            <TextField
                                autoComplete="name"
                                name="name"
                                value={name.value}
                                variant="outlined"
                                required
                                fullWidth
                                id="name"
                                label={t('user.name')}
                                autoFocus
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                    onChange(e, setName);
                                }}
                                helperText={name.error}
                                error={name.error !== null}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label={t('user.email')}
                                name="email"
                                value={email.value}
                                autoComplete="email"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                    onChange(e, setEmail);
                                }}
                                helperText={email.error}
                                error={email.error !== null}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label={t('user.password')}
                                type="password"
                                id="password"
                                value={password.value}
                                autoComplete="current-password"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                    onChange(e, setPassword);
                                }}
                                helperText={password.error}
                                error={password.error !== null}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password_confirmation"
                                value={passwordConfirmation.value}
                                label={t('user.password_confirmation')}
                                type="password"
                                id="password_confirmation"
                                autoComplete="current-password"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                    onChange(e, setPasswordConfirmation);
                                }}
                                helperText={passwordConfirmation.error}
                                error={passwordConfirmation.error !== null}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleSubmit}
                        disabled={loader}
                    >
                        {!loader ? (
                            t("auth.signUp")
                        ) : (
                            <CircularProgress size={20} color="primary"/>
                        )}

                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link href={LOGIN_PATH} variant="body2">
                                {t("auth.signIn")}
                            </Link>
                        </Grid>
                    </Grid>
                </div>
            </div>
            <Box mt={5}>
                <Copyright/>
            </Box>
        </Container>
    );
};

export default React.memo(SignUp);

import React, {Fragment, useCallback, useEffect, useState} from 'react';
import Paper from '@material-ui/core/Paper';
import {CircularProgress, Container, Fab, Tooltip, Typography} from "@material-ui/core";
import {useStyles} from "./style";
import {useDispatch, useSelector} from "react-redux";
import {useTranslation} from "react-i18next";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {createTask, deleteTask, fetchTasks, updateStatus, updateTask} from "../../store/task/actions";
import {RootState} from "../../store/rootReducers";
import Checkbox from '@material-ui/core/Checkbox';
import noTasks from "../../assets/images/empty.png";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import {DialogComponent} from "../../components";
import {Create} from "@material-ui/icons";
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';


const INIT_TASK_FORM = {
    description: {
        value: "",
        error: "",
    }
};

export const TodoList = React.memo(() => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const {t} = useTranslation();

    const [fetchDataLoader, setFetchDataLoader] = React.useState<boolean>(true);
    const [addLoader, setAddLoader] = useState<boolean>(false);
    const [deleteLoader, setDeleteLoader] = useState<boolean>(false);
    const [updateLoader, setUpdateLoader] = useState<boolean>(false);
    const [updateStatusLoader, setUpdateStatusLoader] = useState<boolean>(false);

    const taskState = useSelector((state: RootState) => state.task);
    const [newTask, setNewTask] = useState(INIT_TASK_FORM);
    const [task, setTask] = useState(INIT_TASK_FORM);
    const [deleteTaskId, setDeleteTaskId] = useState<number>(0);
    const [editTaskId, setEditTaskId] = useState<number>(0);
    const [updateStatusId, setUpdateStatusId] = useState<number>(0);

    const fetchData = async () => {
        await dispatch(fetchTasks());
    };

    useEffect(() => {
        setFetchDataLoader(true);
        fetchData();
        setFetchDataLoader(false);
    }, [setFetchDataLoader]);


    const handleChangeNewTaskDescription = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            setNewTask({
                ...newTask,
                description: {
                    ...newTask.description,
                    value: e.target.value,
                    error: "",
                }
            });
        }, [newTask, setNewTask]);

    const handleChangeTaskDescription = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            setTask({
                ...task,
                description: {
                    ...task.description,
                    value: e.target.value,
                    error: "",
                }
            });
        }, [task, setTask]);

    const handleAdd = useCallback(async () => {
        setNewTask({
            ...newTask,
            description: {
                ...newTask.description,
                error:
                    newTask.description.value === "" ?
                        t("validation.description.required")
                        : newTask.description.error,
            }
        });
        if (newTask.description.value !== "") {
            setAddLoader(true);
            await dispatch(createTask(newTask.description.value));
            fetchData();
            setNewTask(INIT_TASK_FORM);
            setAddLoader(false)
        }
    }, [newTask, addLoader, taskState]);

    // update task
    const handleUpdate = useCallback(async () => {
        if (!updateLoader) {
            // validation des champs
            setTask({
                ...task,
                description: {
                    ...task.description,
                    error:
                        task.description.value === "" ?
                            t("validation.description.required")
                            : task.description.error,
                }
            });
            if (task.description.value !== "") {
                setUpdateLoader(true);
                await dispatch(updateTask(editTaskId, task.description.value));
                setEditTaskId(0);
                setUpdateLoader(false);
                fetchData();
            }

        }
    }, [editTaskId, task, setUpdateLoader]);

    const handleDelete = useCallback(async () => {
        if (!deleteLoader) {
            setDeleteLoader(true);
            await dispatch(deleteTask(deleteTaskId));
            setDeleteTaskId(0);
            setDeleteLoader(false);
            fetchData();
        }

    }, [setDeleteLoader, deleteTaskId]);

    const handleUpdateStatus = useCallback(async (id) => {
        if (!updateStatusLoader) {
            setUpdateStatusId(id);
            setUpdateStatusLoader(true);
            await dispatch(updateStatus(id));
            setUpdateStatusLoader(false);
            setUpdateStatusId(0);
            fetchData();
        }

    }, [setUpdateStatusLoader, setUpdateStatusId]);

    // handle open update task dialog
    const handleOpenUpdateDialog = useCallback(
        (id, description) => {
            setEditTaskId(id);
            setTask((prevState) => ({
                ...prevState,
                description: {
                    ...prevState.description,
                    value: description,
                }
            }));
        },
        [setEditTaskId, setTask]
    );
    // handle close update task dialog
    const handleCloseUpdateDialog = useCallback(() => {
        setEditTaskId(0);
        setTask(INIT_TASK_FORM);
    }, [setEditTaskId, setTask]);

    return (
        <Container component="main">
            <Paper elevation={3} className={classes.paper}>
                <div className={classes.section}>
                    <Grid container spacing={1} alignItems="flex-end">
                        <Grid item>
                            <AddIcon/>
                        </Grid>
                        <Grid item className={classes.newTaskContainer}>
                            <TextField id="new-task-description" className={classes.newTaskInput}
                                       label={t("global.description")}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                           handleChangeNewTaskDescription(e);
                                       }}
                                       value={newTask.description.value}
                                       helperText={newTask.description.error}
                                       error={newTask.description.error !== ""}
                            />
                        </Grid>
                        <Grid>
                            <Button
                                variant="contained"
                                color="primary"
                                size="large"
                                className={classes.button}
                                onClick={handleAdd}
                                startIcon={<SaveIcon/>}
                                disabled={addLoader}
                            >
                                {t("global.add")}
                                {addLoader && (
                                    <CircularProgress size={20} color="primary"/>
                                )}
                            </Button>
                        </Grid>
                    </Grid>
                </div>

                <div className={classes.margin}>
                    {
                        !taskState.data.length && <Fragment>
                            <Grid container spacing={1} alignItems="flex-end">
                                <div className={classes.center}>
                                    <img alt={"..."} src={noTasks} className={classes.image}/>
                                    <Typography variant="h6" noWrap className={classes.textDisabled}>
                                        {t('global.nothingToShowTitle')}</Typography>
                                    <Typography variant="h6" noWrap className={classes.textDisabled}>
                                        {t('global.nothingToShowBody')}
                                    </Typography>

                                </div>
                            </Grid>
                        </Fragment>
                    }
                    {!fetchDataLoader ? <Fragment>
                            {taskState.data.map(e => (
                                <Grid key={e.id} container spacing={1} className={classes.task}>
                                    <Grid item md={8} xs={8}>
                                        <FormControlLabel control={<Checkbox
                                            checked={e.done_at !== null}
                                            icon={<EmojiEventsIcon fontSize="large"/>}
                                            checkedIcon={<EmojiEventsIcon fontSize="large"/>}
                                            name="checkedH"
                                            color={"primary"}
                                            onChange={() => handleUpdateStatus(e.id)}
                                        />} label={e.description} className={e.done_at !== null ? classes.done : ""}/>
                                    </Grid>

                                    <Grid item md={2} xs={2}>
                                        {(updateStatusLoader && updateStatusId === e.id) && (

                                            <CircularProgress size={20} color="primary"/>
                                        )}
                                        {
                                            e.done_at !== null && (
                                                <p color={"default"}>
                                                    {t("global.doneAt")} {e.done_at}
                                                </p>
                                            )
                                        }
                                    </Grid>
                                    <Grid item md={2} xs={2}>
                                        <Tooltip title={t("global.update") as string} arrow>
                                            <Fab
                                                className={classes.buttonEdit}
                                                onClick={() => handleOpenUpdateDialog(e.id, e.description)}
                                            >
                                                <Create/>
                                            </Fab>
                                        </Tooltip>

                                        <Tooltip title={t("global.delete") as string} arrow>
                                            <Fab
                                                className={classes.buttonDelete}
                                                onClick={() => setDeleteTaskId(e.id)}
                                            >
                                                <DeleteForeverIcon/>
                                            </Fab>
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                            ))}
                            {editTaskId !== 0 && (
                                <DialogComponent
                                    title={t("global.updateTask")}
                                    open={editTaskId !== 0}
                                    handleClose={handleCloseUpdateDialog}
                                    handleCancel={handleCloseUpdateDialog}
                                    continueTitle={t("global.update")}
                                    cancelTitle={t("global.cancel")}
                                    handleContinue={handleUpdate}
                                    spinner={updateLoader}
                                >
                                    <TextField id="edit-task-description" className={classes.newTaskInput}
                                               label={t("global.description")}
                                               onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                                   handleChangeTaskDescription(e);
                                               }}
                                               value={task.description.value}
                                               helperText={task.description.error}
                                               error={task.description.error !== ""}
                                    />
                                </DialogComponent>
                            )}
                            {deleteTaskId !== 0 && (
                                <DialogComponent
                                    title={t("global.deleteTask")}
                                    open={deleteTaskId !== 0}
                                    handleClose={() => {
                                        setDeleteTaskId(0);

                                    }}
                                    spinner={deleteLoader}
                                    handleCancel={() => {
                                        setDeleteTaskId(0);
                                    }}
                                    handleContinue={handleDelete}
                                    continueTitle={t("global.delete")}
                                    cancelTitle={t("global.cancel")}
                                >
                                    <Typography>{t("global.areYouSure")}</Typography>
                                </DialogComponent>
                            )}
                        </Fragment> :
                        <CircularProgress size={70} color="primary"/>
                    }
                </div>
            </Paper>
        </Container>
    );
});
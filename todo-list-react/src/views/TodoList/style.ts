import {makeStyles} from "@material-ui/core/styles";
import {COLORS} from '../../utils/colors';

export const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        '& > *': {
            margin: "50px",
        },
        '& > * + *': {
            margin: theme.spacing(2),
        },
    },
    paper: {
        padding: '50px',
        textAlign: 'center',
        backgroundColor: COLORS.backgroud,
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    title: {
        fontSize: 20,
        textAlign: 'left',
    },
    section: {
        paddingBottom: '50px',
    },
    margin: {
        margin: theme.spacing(1),
    },
    button: {
        margin: theme.spacing(1),
    },
    task: {
        margin: theme.spacing(2),
        paddingTop: '15px',
        // alignItems: 'center',
      justifyContent: 'space-between',
      display: 'flex',
      textAlign: 'left',

},
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    newTaskContainer: {
        width: '75%',
    },
    newTaskInput: {
        width: '100%'
    },
    image: {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '50%',
    },
    center: {
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    textDisabled: {
        color: COLORS.grey,
    },
    done: {
        textDecorationLine: 'line-through',
    },
    buttonDelete: {
        borderColor: COLORS.red,
        borderWidth: "2px",
        borderStyle: "solid",
        borderRadius: "50%",
        height: "40px",
        width: "40px",
        cursor: "pointer",
        color: COLORS.red,
        backgroundColor: "transparent",
        margin: theme.spacing(1),
        "&:hover": {
            color: "white",
            backgroundColor: COLORS.red,
        },

    },
    buttonEdit: {
        borderColor: COLORS.green,
        borderWidth: "2px",
        borderStyle: "solid",
        borderRadius: "50%",
        height: "40px",
        width: "40px",
        cursor: "pointer",
        color: COLORS.green,
        backgroundColor: "transparent",
        margin: theme.spacing(1),
        "&:hover": {
            color: "white",
            backgroundColor: COLORS.green,
        },
    },
}));

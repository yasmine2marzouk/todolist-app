import { makeStyles } from "@material-ui/core/styles";
import background_1 from "../../assets/images/background_2.png";

export const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flex: "1",
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    justifyContent: "center",
    alignItems: "center",
    backgroundImage: `url(${background_1})`,
    backgroundSize: "cover",
    backgroundColor: theme.palette.primary.main,
  },
  logo: {
    height: 100,
    width: "200vw",
    marginBottom: 30,
  },
  loader: {
    height: 10,
    backgroundColor: theme.palette.primary.main,
    width: "40vw",
    borderRadius: 10,
    overflow: "hidden",
    opacity: 0.5,
  },
  subLoader: {
    height: "100%",
    width: 0,
    backgroundColor: "white",
    opacity: 1,
    borderRadius: 10,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: 2000,
    }),
  },
  subLoaderCompleted: {
    height: "100%",
    width: "40vw",
    backgroundColor: "white",
    opacity: 1,
    borderRadius: 10,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: 2000,
    }),
  },
}));

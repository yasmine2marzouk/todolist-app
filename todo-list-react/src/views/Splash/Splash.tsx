import React, { useEffect, useState } from 'react';
import { useStyles } from './style';

export const Splash = React.memo(() => {
    const classes = useStyles();
    const [loader, setLoader] = useState<boolean>(false);
    useEffect(() => {
        setLoader(true)
    }, []);
    return <div className={classes.container}>
        <div className={classes.loader}>
            <div className={loader ? classes.subLoaderCompleted : classes.subLoader} />
        </div>
    </div>
})
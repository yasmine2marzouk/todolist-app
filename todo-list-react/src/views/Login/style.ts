import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  loginFormContainer: {
    [theme.breakpoints.down("xs")]: {
      padding: 10,
    },
    paddingRight: 60,
    paddingLeft: 60,
    paddingBottom: 20,
  },
  remember_me: {
    margin: 0,
  },
  title: {
    [theme.breakpoints.down("xs")]: {
      padding: 10,
    },
    paddingRight: 60,
    paddingLeft: 60,
    paddingTop: 20,
    paddingBottom: 20,
  },
  resetPasswordContainer: {
    [theme.breakpoints.down("xs")]: {
      padding: 10,
      justifyContent: "center",
      alignItems: "center",
    },
    alignItems: "flex-end",
    paddingBottom: 20,
  },
  container: {
    width: "100vw",
    height: "100vh",
    display: "flex",
    alignItems: "center",
  },
  button: {
    width: "30%",
    backgroundColor: "rgba(0, 0, 0, 0.1)",
    minWidth: 120
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

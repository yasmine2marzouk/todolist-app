import {
    Box,
    Button,
    Checkbox,
    CircularProgress,
    Container,
    CssBaseline,
    FormControlLabel,
    Grid,
    TextField,
    Typography,
} from "@material-ui/core";
import {AxiosError, AxiosResponse} from "axios";
import React, {useCallback, useState} from "react";
import {useTranslation} from "react-i18next";
import {useDispatch} from "react-redux";
import Link from '@material-ui/core/Link';
import {login} from "../../store/auth/actions";
import {InputInterface} from "../../utils/common";
import {isEmail} from "../../utils/helpers";
import {SIGN_UP_PATH} from "../../utils/paths";
import {useStyles} from "./style";
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

interface LoginProps {
}

const Login = (props: LoginProps) => {
    const classes = useStyles();
    const {t} = useTranslation();
    const dispatch = useDispatch();
    const [email, setEmail] = useState<InputInterface<string>>({
        value: "",
        error: null,
    });
    const [password, setPassword] = useState<InputInterface<string>>({
        value: "",
        error: null,
    });

    const [rememberMe, setRememberMe] = useState<boolean>(false);
    const [loader, setLoader] = useState<boolean>(false);

    const onChange = useCallback(
        (
            event: React.ChangeEvent<HTMLInputElement>,
            setState: React.Dispatch<React.SetStateAction<InputInterface<string>>>
        ) => {
            setState((e) => ({...e, value: event.target.value, error: null}));
        },
        []
    );

    const handleSubmit = (): void => {
        if (email.value === "")
            setEmail({...email, error: t("validation.email.required")});
        if (email.value !== "" && !isEmail(email.value))
            setEmail({...email, error: t("validation.email.email")});
        if (password.value === "")
            setPassword({...password, error: t("validation.password.required")});
        if (email.value !== "" && isEmail(email.value) && password.value !== "") {
            setLoader(true);
            dispatch(
                login(
                    {
                        email: email.value,
                        password: password.value,
                        remember: rememberMe,
                    },
                    (isSuccess: boolean, axios: AxiosError | AxiosResponse) => {
                        if (!isSuccess) {
                            const error = axios as AxiosError;
                            if (error.response?.status === 422) {
                                setEmail({
                                    ...email,
                                    error: error.response?.data.errors.email
                                        ? t(`validation.${error.response?.data.errors.email[0]}`)
                                        : email.error,
                                });
                                setPassword({
                                    ...password,
                                    error: error.response?.data.errors.password
                                        ? t(`validation.${error.response?.data.errors.password[0]}`)
                                        : password.error,
                                });

                            }
                        }
                        setLoader(false);
                    }
                )
            );
        }
    };

    function Copyright() {
        return (
            <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                <Link color="inherit" href="">
                    {t('global.app_name')}
                </Link>{' '}
                {new Date().getFullYear()}
                {'.'}
            </Typography>
        );
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    {t("auth.login")}
                </Typography>
                <div className={classes.form}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        value={email.value}
                        required
                        fullWidth
                        id="email"
                        label={t("user.email")}
                        name="email"
                        type="email"
                        autoComplete="email"
                        autoFocus
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                            onChange(e, setEmail);
                        }}
                        helperText={email.error}
                        error={email.error !== null}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        value={password.value}
                        required
                        fullWidth
                        name="password"
                        label={t("user.password")}
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                            onChange(e, setPassword);
                        }}
                        helperText={password.error}
                        error={password.error !== null}
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                color="primary"
                                value={rememberMe}
                                onChange={() => setRememberMe(!rememberMe)}
                            />
                        }
                        label={t("auth.rememberMe")}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleSubmit}
                        disabled={loader}
                    >
                        {!loader ? (
                            t("auth.login")
                        ) : (
                            <CircularProgress size={20} color="primary"/>

                        )}
                    </Button>
                    <Grid container>
                        <Grid item>
                            <Link href={SIGN_UP_PATH} variant="body2">
                                {t("auth.signUpHere")}
                            </Link>
                        </Grid>
                    </Grid>
                </div>
            </div>
            <Box mt={8}>
                <Copyright/>
            </Box>
        </Container>
    );
};

export default React.memo(Login);

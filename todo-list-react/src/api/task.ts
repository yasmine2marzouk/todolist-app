import axios from "axios";
import {AXIOS_CONFIG, TOKEN} from "../utils/constants";
import {DELETE_TASK_ENDPOINT, FETCH_TASKS_ENDPOINT, UPDATE_TASK_STATUS_ENDPOINT} from "../utils/endpoints";
import {Task} from "../models/Task";

// fetch ws consume
const fetch = () => {
    const token = localStorage.getItem(TOKEN);
    const config = {
        ...AXIOS_CONFIG,
        headers: {
            ...AXIOS_CONFIG.headers,
            Authorization: `Bearer ${token}`,
        },
    };
    return axios.get(FETCH_TASKS_ENDPOINT, config).then((response) => {
        const data = response.data as Array<Task>;
        return data;
    });
};

// create ws consume
const create = (data: { description: string }) => {
    const token = localStorage.getItem(TOKEN);
    const config = {
        ...AXIOS_CONFIG,
        headers: {
            ...AXIOS_CONFIG.headers,
            Authorization: `Bearer ${token}`,
        }
    };
    return axios
        .post(FETCH_TASKS_ENDPOINT, data, config)
        .then((response) => {
          return response.data;
        });
};

// update ws consume
const updateTask = (id: number, data: { description: string }) => {
    const token = localStorage.getItem(TOKEN);
    const config = {
        ...AXIOS_CONFIG,
        headers: {
            ...AXIOS_CONFIG.headers,
            Authorization: `Bearer ${token}`,
        }
    };
    const url = DELETE_TASK_ENDPOINT.replace("{id}", `${id}`);
    return axios
        .put(url, data, config)
        .then((response) => {
          return response.data;
        });
};

// delete ws consume
const removeTask = (id: number) => {
    const token = localStorage.getItem(TOKEN);
    const config = {
        ...AXIOS_CONFIG,
        headers: {
            ...AXIOS_CONFIG.headers,
            Authorization: `Bearer ${token}`,
        }
    };
    const url = DELETE_TASK_ENDPOINT.replace("{id}", `${id}`);
    return axios.delete(url, config).then((response) => response.data);
};

// delete ws consume
const updateStatus = (id: number) => {
    const token = localStorage.getItem(TOKEN);
    const config = {
        ...AXIOS_CONFIG,
        headers: {
            ...AXIOS_CONFIG.headers,
            Authorization: `Bearer ${token}`,
        }
    };
    const url = UPDATE_TASK_STATUS_ENDPOINT.replace("{id}", `${id}`);
    return axios.put(url, null, config).then((response) => response.data);
};

export const task = {fetch, create, updateTask, removeTask, updateStatus};

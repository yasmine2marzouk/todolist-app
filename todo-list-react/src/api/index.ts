import { auth } from "./auth";
import { task } from "./task";

export const api = {auth, task};

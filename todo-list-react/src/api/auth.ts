import axios from "axios";
import {AXIOS_CONFIG, TOKEN,} from "../utils/constants";
import {GET_AUTH_ENDPOINT, LOGIN_ENDPOINT, LOGOUT_ENDPOINT, SIGN_UP_ENDPOINT} from "../utils/endpoints";
import {history} from "../utils/history";

// login ws consume
const login = (data: { email: string; password: string }) => {
    return axios({
        ...AXIOS_CONFIG,
        data: {...data},
        url: LOGIN_ENDPOINT,
        method: "POST",
    });
};

// signup ws consume
const signup = (data: {
    email: string;
    name: string;
    password: string;
    password_confirmation: string;
}) => {
    return axios({
        ...AXIOS_CONFIG,
        data,
        url: SIGN_UP_ENDPOINT,
        method: "POST",
    });
};

// get auth user ws consume
const getAuth = () => {
    const token = localStorage.getItem(TOKEN);
    return axios({
        ...AXIOS_CONFIG,
        url: GET_AUTH_ENDPOINT,
        method: "GET",
        headers: {
            ...AXIOS_CONFIG.headers,
            Authorization: `Bearer ${token}`,
        },
    });
};

// logout ws consume
const logout = () => {
    const token = localStorage.getItem(TOKEN);
    localStorage.removeItem(TOKEN);
    history.push(LOGIN_ENDPOINT);
    axios({
        ...AXIOS_CONFIG,
        url: LOGOUT_ENDPOINT,
        method: "POST",
        headers: {
            ...AXIOS_CONFIG.headers,
            Authorization: `Bearer ${token}`,
        },
    });
};

export const auth = {
    login,
    logout,
    signup,
    getAuth,
};
